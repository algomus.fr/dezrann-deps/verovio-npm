This is an NPM of a custom version of the Verovio JavaScript toolkit with Humdrum support.

[Verovio](https://www.verovio.org/) is a fast, portable and lightweight library for engraving [Music Encoding Initiative (MEI)](http://www.music-encoding.org) music scores into SVG.

You can see Verovio with Humdrum support running in the [Verovio Humdrum Viewer](http://verovio.humdrum.org/).

This version of the toolkit was compiled from the sources (https://github.com/rism-digital/verovio).

## Usage

```javascript
var verovio = require( 'verovio-humdrum' );
var fs = require( 'fs' );

/* Wait for verovio to load */
verovio.module.onRuntimeInitialized = function ()
{
    /* create the toolkit instance */
    var vrvToolkit = new verovio.toolkit();
    /* read the Humdrum file */
	krn = fs.readFileSync("hello.krn");
    /* load the Humdrum data as string into the toolkit */
	vrvToolkit.loadData(krn.toString());
    /* render the fist page as SVG */
	svg = vrvToolkit.renderToSVG(1, {});
    /* save the SVG into a file */
	fs.writeFileSync("hello.svg", svg);
}
```

## Compilation instructions

### Install emscripten (https://emscripten.org/docs/getting_started/downloads.html)

```sh
# Get the emsdk repo
git clone https://github.com/emscripten-core/emsdk.git

# Enter that directory
cd emsdk

# Download and install the latest SDK tools.
./emsdk install latest

# Make the "latest" SDK "active" for the current user. (writes .emscripten file)
./emsdk activate latest

# Activate PATH and other environment variables in the current terminal
source ./emsdk_env.sh
```

### Clone github repository

```sh
git clone https://github.com/rism-digital/verovio.git
```

### build the npm package

```sh
cd verovio/emscripten

./buildToolkit -w
```

### copy npm dir to the git repo

```sh
cp -Rf npm-hum/* /path/to/git/repo/dir
```

### commit and push

```sh
cd /path/to/git/repo/dir

git add -u

git commit -m "message which describe the intension of this update"

git push
```

